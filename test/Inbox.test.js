const assert = require('assert')
const ganache = require('ganache-cli')
const Web3 = require('web3')
const web3 = new Web3(ganache.provider())
// const web3 = new Web3()
const { interface, bytecode } = require('../compile')

const INITIAL_MESSAGE = 'Hi there!';
let accounts
let inbox

beforeEach(async () => {
    // Get List of all accounts
    accounts = await web3.eth.getAccounts()
    // Use one of those accounts to deploy the contracts
    inbox = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({data: bytecode, arguments: [INITIAL_MESSAGE]})
    .send({from: accounts[0], gas: '1000000'})
})

describe('Inbox', () => {
    it('Deploys a contract', () => {
        assert.ok(inbox.options.address);
    })

    it('has a default message', async () => {
        const message = await inbox.methods.message().call();
        assert.strictEqual(message, INITIAL_MESSAGE);
    });

    it('can change the message', async() => {
        const updatedMessage = 'Bye there!'
        const txHash = await inbox.methods.setMessage(updatedMessage)
        .send({
            from: accounts[0]
        })

        const message = await inbox.methods.message().call();
        assert.strictEqual(message, updatedMessage)
    });
})
